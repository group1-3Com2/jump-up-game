void jumpOn(int16_t x, int16_t y){
  for(int i=10-x; i<10; i++){
    for(int j=39-y; j<39; j++){
      oled.drawPixel(i,j,BLACK);
    }
  }
  for(int i=10-x; i<10; i++){
    for(int j=29-y; j<29; j++){
      oled.drawPixel(i,j,WHITE);
    }
  }
  oled.display();
}

void jumpDown(int16_t x, int16_t y){
  for(int i=10-x; i<10; i++){
    for(int j=39-y; j<39; j++){
      oled.drawPixel(i,j,WHITE);
    }
  }
  for(int i=10-x; i<10; i++){
    for(int j=29-y; j<29; j++){
      oled.drawPixel(i,j,BLACK);
    }
  }
  oled.display();
}