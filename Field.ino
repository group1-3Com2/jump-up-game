void Field() {
  for (int i = 40; i < 70; i++) {
    if (i <= 50) {
      oled.drawFastHLine(0, i, 128, WHITE);
    } else {
      (i % 2 == 0) ? oled.drawFastHLine(0, i, 128, WHITE) : oled.drawFastHLine(0, i, 128, BLACK);
    }
  }
}

void spawnPosition(int16_t x, int16_t y) {
  for(int i=10-x; i<10; i++){
    for(int j=39-y; j<39; j++){
      oled.drawPixel(i,j,WHITE);
      oled.display();
    }
  }
}

void gameOver(int16_t x, int16_t y){
  resetRGB();
  digitalWrite(rgb[2], HIGH);
  for(int j=39; j>=39-y; j--){
    for(int i=10; i>=10-x; i--){
      oled.drawPixel(i,j,BLACK);
      oled.display();
    }
  }
  oled.setTextSize(2);
  oled.setTextColor(WHITE);
  oled.setCursor(10,10);
  oled.println("GAME OVER");
  oled.display();

  while(true);
}