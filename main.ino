#include <Adafruit_SSD1306.h>
#include <Stepper.h>

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64

const int stepsPerRevolution = 50; 

// objs instance
Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
Stepper myStepper(stepsPerRevolution, 9, 10, 11, 12);

// used pin of Arduino
const int pinSoundDetect = 2; 
int rgb[3] = {3,4,5};
int buzzer = 6;
int pinTurn = 8;

// other variable
int lengthLimited = SCREEN_WIDTH;
int countMove = 0;
bool countStatus = false;
bool onceJump = true;

struct player {
  int16_t weight = 6;
  int16_t height = 6;
};
player p;

struct obstacle {
  int weight = 3;
  int height = 3;
};
obstacle o;


void setup() {
  Serial.begin(9600);
  pinMode(pinSoundDetect, INPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(pinTurn, INPUT);
  
  for (int i=0; i<3; i++){
    pinMode(rgb[i], OUTPUT);
  }

  if (!oled.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    while (true);
  }

  myStepper.setSpeed(50);

  oled.clearDisplay();
  Field();
  spawnPosition(p.weight, p.height);
  // gameOver(p.weight, p.height);
  oled.display();
}

void loop() {
  if (digitalRead(pinTurn)){
    myStepper.step(-stepsPerRevolution);
  }

  resetRGB();
  digitalWrite(rgb[0], HIGH);

  if (digitalRead(pinSoundDetect)){
    digitalWrite(buzzer, HIGH);
    countStatus = true;
    jumpOn(p.weight, p.height);
  }

  if (countStatus){
    countMove++;
    if (countMove >= 25){
      resetRGB();
      digitalWrite(rgb[1], HIGH);
      digitalWrite(rgb[2], HIGH);
    }
    if (countMove == 50){ 
      jumpDown(p.weight, p.height);
      countMove = 0;
      countStatus = false;
    }
  }

  // reset length
  if(lengthLimited <= 0) {
    lengthLimited = SCREEN_WIDTH;
  }
  
  // block obstacle
  for (int j=39-o.height; j<39; j++){
    if (oled.getPixel((int16_t )lengthLimited,(int16_t )j)){
      gameOver(p.weight, p.height);
    }
    oled.drawPixel(lengthLimited,j,WHITE);
  }
  oled.display();
  for (int j=39-o.height; j<39; j++){
    oled.drawPixel(lengthLimited,j,BLACK);
  }
  oled.display();

  lengthLimited--;
}

void resetRGB(){
  digitalWrite(rgb[0], LOW);
  digitalWrite(rgb[1], LOW);
  digitalWrite(rgb[2], LOW);
}
